# Mac

A small script to quickly get your macOS machine ready for web development.

## Install

Download the script:

```sh
curl --remote-name https://bitbucket.org/fasttrackdevteam/mac-dev-setup/raw/master/mac
```

Execute the downloaded script:

```sh
sh mac 2>&1 | tee ~/mac.log
```

Optionally, review the log:

```sh
less ~/mac.log
```

## What it sets up

macOS tools:

- [Homebrew] for managing operating system libraries.

[homebrew]: http://brew.sh/

Unix tools:

- [Git] for version control
- [OpenSSL] for Transport Layer Security (TLS)
- [The Silver Searcher] for finding things in files
- [Tmux] for saving project state and switching between projects
- [Watchman] for watching for filesystem events
- [Zsh] as your shell (with [Oh My ZSH][omz] and [Powerlevel9k][pl9k] theme)

[git]: https://git-scm.com/
[openssl]: https://www.openssl.org/
[the silver searcher]: https://github.com/ggreer/the_silver_searcher
[tmux]: http://tmux.github.io/
[watchman]: https://facebook.github.io/watchman/
[zsh]: http://www.zsh.org/
[omz]: https://github.com/robbyrussell/oh-my-zsh
[pl9k]: https://github.com/bhilburn/powerlevel9k

Heroku tools:

- [Heroku CLI]

[heroku cli]: https://devcenter.heroku.com/articles/heroku-cli

GitHub tools:

- [Hub] for interacting with the GitHub API

[hub]: http://hub.github.com/

Image tools:

- [ImageMagick] for cropping and resizing images

Programming languages, package managers, and configuration:

- [Node.js] and [NPM], for running apps and installing JavaScript packages
- [Yarn] for managing JavaScript packages
- [Terraform] for creating infrastructure with code
- [Chamber] for managing secrets in SSM
- [Go] for writing general-purpose code
- [Vue] cli for frontend development
- [PHP] to run legacy services

[imagemagick]: http://www.imagemagick.org/
[node.js]: http://nodejs.org/
[npm]: https://www.npmjs.org/
[yarn]: https://yarnpkg.com/en/
[terraform]: https://www.terraform.io/
[chamber]: https://github.com/segmentio/chamber
[go]: https://golang.org/
[vue]: https://vuejs.org/
[php]: https://www.php.net/

Databases:

- [Postgres] for storing relational data
- [Redis] for storing key-value data

[postgres]: http://www.postgresql.org/
[redis]: http://redis.io/

Applications:

- [iTerm2] for a better terminal
- [Visual Studio Code][vscode] for writing code
- [ngrok] to tunnel your localhost
- [Docker] for containerizing applications
- [Postman] to test APIs
- [Sequel-Pro] to manage databases
- [Gas Mask][gasmask] to manage host file entries
- [1Password] to manage passwords
- [Google Chrome][chrome] as primary browser
- [Firefox] as secondary browser
- [Slack] for team communication
- [Skype] for external communication
- [Spotify] to stream music

[iterm2]: https://www.iterm2.com/
[vscode]: https://code.visualstudio.com/
[ngrok]: https://ngrok.com/
[docker]: https://www.docker.com/
[postman]: https://www.getpostman.com/
[sequel-pro]: https://www.sequelpro.com/
[gasmask]: https://github.com/2ndalpha/gasmask
[1password]: https://1password.com/
[chrome]: https://www.google.com/chrome/
[firefox]: https://www.mozilla.org/en-GB/firefox/new/?redirect_source=firefox-com
[slack]: https://slack.com/
[skype]: https://www.skype.com/en/
[spotify]: https://www.spotify.com
[mdct]: https://github.com/MartinSeeler/iterm2-material-design
[nerdfont]: https://github.com/ryanoasis/nerd-fonts

It should take less than 15 minutes to install (depends on your machine).
